from . import curve, lattice, maths, misc, post_gen

modules = [
    curve,
    lattice,
    maths,
    misc,
    post_gen,
]